# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-12 02:01+0000\n"
"PO-Revision-Date: 2023-05-09 22:32+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: contents/ui/ConnectionItem.qml:61
#, kde-format
msgid "Connect"
msgstr "Conectar"

#: contents/ui/ConnectionItem.qml:61
#, kde-format
msgid "Disconnect"
msgstr "Desconectar"

#: contents/ui/ConnectionItem.qml:86
#, kde-format
msgid "Show Network's QR Code"
msgstr "Amosar el códigu QR de la rede"

#: contents/ui/ConnectionItem.qml:91
#, kde-format
msgid "Configure…"
msgstr "Configurar…"

#: contents/ui/ConnectionItem.qml:128
#, kde-format
msgid "Speed"
msgstr "Velocidá"

#: contents/ui/ConnectionItem.qml:133
#, kde-format
msgid "Details"
msgstr "Detalles"

#: contents/ui/ConnectionItem.qml:176
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Current download speed is %1 kibibytes per second; current upload speed is "
"%2 kibibytes per second"
msgstr ""

#: contents/ui/ConnectionItem.qml:289
#, kde-format
msgid "Connected, ⬇ %1/s, ⬆ %2/s"
msgstr ""

#: contents/ui/ConnectionItem.qml:293
#, kde-format
msgid "Connected"
msgstr ""

#: contents/ui/ConnectionListPage.qml:34
#, kde-format
msgid "You need to log in to this network"
msgstr ""

#: contents/ui/ConnectionListPage.qml:38
#, kde-format
msgctxt "@action:button"
msgid "Log in"
msgstr ""

#: contents/ui/ConnectionListPage.qml:114
#, kde-format
msgid "Airplane mode is enabled"
msgstr "El mou Avión ta activáu"

#: contents/ui/ConnectionListPage.qml:118
#, kde-format
msgid "Wireless and mobile networks are deactivated"
msgstr "La rede ensin filos ya la móvil tán desactivaes"

#: contents/ui/ConnectionListPage.qml:120
#, kde-format
msgid "Wireless is deactivated"
msgstr "La rede ensin filos ta desactivada"

#: contents/ui/ConnectionListPage.qml:123
#, kde-format
msgid "Mobile network is deactivated"
msgstr "La rede móvil ta desactivada"

#: contents/ui/ConnectionListPage.qml:126
#, kde-format
msgid "No matches"
msgstr "Nun hai nenguna coincidencia"

#: contents/ui/ConnectionListPage.qml:128
#, kde-format
msgid "No available connections"
msgstr ""

#: contents/ui/DetailsText.qml:45
#, kde-format
msgid "Copy"
msgstr ""

#: contents/ui/main.qml:27
#, kde-format
msgid "Networks"
msgstr "Redes"

#: contents/ui/main.qml:36
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn off Airplane Mode"
msgstr ""

#: contents/ui/main.qml:38
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn on Airplane Mode"
msgstr ""

#: contents/ui/main.qml:67 contents/ui/Toolbar.qml:75
#, kde-format
msgid "Enable Wi-Fi"
msgstr "Activar la Wi-Fi"

#: contents/ui/main.qml:78
#, kde-format
msgid "Enable Mobile Network"
msgstr "Activar la rede móvil"

#: contents/ui/main.qml:90
#, kde-format
msgid "Enable Airplane Mode"
msgstr "Activar el mou Avión"

#: contents/ui/main.qml:102
#, kde-format
msgid "Open Network Login Page…"
msgstr ""

#: contents/ui/main.qml:112
#, kde-format
msgid "&Configure Network Connections…"
msgstr "&Configurar les conexones a la rede…"

#: contents/ui/PasswordField.qml:16
#, kde-format
msgid "Password…"
msgstr "Contraseña…"

#: contents/ui/PopupDialog.qml:51
#, kde-format
msgctxt "@action:button"
msgid "Return to Network Connections"
msgstr ""

#: contents/ui/ShareNetworkQrCodePage.qml:31
#, kde-format
msgid "Scan this QR code with another device to connect to the \"%1\" network."
msgstr "Escania esti códigu QR con otru preséu pa conectate a la rede «%1»."

#: contents/ui/Toolbar.qml:124
#, kde-format
msgid "Enable mobile network"
msgstr ""

#: contents/ui/Toolbar.qml:149
#, kde-kuit-format
msgctxt "@info"
msgid "Disable airplane mode<nl/><nl/>This will enable Wi-Fi and Bluetooth"
msgstr ""

#: contents/ui/Toolbar.qml:150
#, kde-kuit-format
msgctxt "@info"
msgid "Enable airplane mode<nl/><nl/>This will disable Wi-Fi and Bluetooth"
msgstr ""

#: contents/ui/Toolbar.qml:161
#, kde-format
msgid "Hotspot"
msgstr "Puntu Wi-Fi"

#: contents/ui/Toolbar.qml:185 contents/ui/Toolbar.qml:196
#, kde-format
msgid "Disable Hotspot"
msgstr ""

#: contents/ui/Toolbar.qml:190 contents/ui/Toolbar.qml:196
#, kde-format
msgid "Create Hotspot"
msgstr ""

#: contents/ui/Toolbar.qml:228
#, kde-format
msgid "Configure network connections…"
msgstr ""

#: contents/ui/TrafficMonitor.qml:35 contents/ui/TrafficMonitor.qml:106
#, kde-format
msgid "/s"
msgstr "/s"

#: contents/ui/TrafficMonitor.qml:87
#, kde-format
msgid "Upload"
msgstr ""

#: contents/ui/TrafficMonitor.qml:87
#, kde-format
msgid "Download"
msgstr ""
