# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
# Lorenz Adam Damara <lorenzrenz@gmail.com>, 2018.
# Wantoyo <wantoyek@gmail.com>, 2018, 2019, 2020, 2021, 2022.
# Linerly <linerly@protonmail.com>, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-12 02:01+0000\n"
"PO-Revision-Date: 2023-03-03 09:54+0700\n"
"Last-Translator: Linerly <linerly@protonmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: contents/ui/ConnectionItem.qml:61
#, kde-format
msgid "Connect"
msgstr "Hubungkan"

#: contents/ui/ConnectionItem.qml:61
#, kde-format
msgid "Disconnect"
msgstr "Putuskan"

#: contents/ui/ConnectionItem.qml:86
#, kde-format
msgid "Show Network's QR Code"
msgstr "Tampilkan Kode QR Jaringan"

#: contents/ui/ConnectionItem.qml:91
#, kde-format
msgid "Configure…"
msgstr "Konfigurasikan..."

#: contents/ui/ConnectionItem.qml:128
#, kde-format
msgid "Speed"
msgstr "Kecepatan"

#: contents/ui/ConnectionItem.qml:133
#, kde-format
msgid "Details"
msgstr "Detail"

#: contents/ui/ConnectionItem.qml:176
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Current download speed is %1 kibibytes per second; current upload speed is "
"%2 kibibytes per second"
msgstr ""
"Kecepatan unduhan saat ini %1 kibibita per detik; kecepatan unggahan saat "
"ini %2 kibibita per detik"

#: contents/ui/ConnectionItem.qml:289
#, kde-format
msgid "Connected, ⬇ %1/s, ⬆ %2/s"
msgstr "Terhubung, ⬇ %1/dt,  ⬆ %2/dt"

#: contents/ui/ConnectionItem.qml:293
#, kde-format
msgid "Connected"
msgstr "Terhubung"

#: contents/ui/ConnectionListPage.qml:34
#, kde-format
msgid "You need to log in to this network"
msgstr ""

#: contents/ui/ConnectionListPage.qml:38
#, kde-format
msgctxt "@action:button"
msgid "Log in"
msgstr ""

#: contents/ui/ConnectionListPage.qml:114
#, kde-format
msgid "Airplane mode is enabled"
msgstr "Mode pesawat diaktifkan"

#: contents/ui/ConnectionListPage.qml:118
#, kde-format
msgid "Wireless and mobile networks are deactivated"
msgstr "Nirkabel dan jaringan seluler dinonaktifkan"

#: contents/ui/ConnectionListPage.qml:120
#, kde-format
msgid "Wireless is deactivated"
msgstr "Nirkabel dinonaktifkan"

#: contents/ui/ConnectionListPage.qml:123
#, kde-format
msgid "Mobile network is deactivated"
msgstr "Jaringan seluler dinonaktifkan"

#: contents/ui/ConnectionListPage.qml:126
#, kde-format
msgid "No matches"
msgstr "Tidak ada yang cocok"

#: contents/ui/ConnectionListPage.qml:128
#, kde-format
msgid "No available connections"
msgstr "Tidak ada koneksi yang tersedia"

#: contents/ui/DetailsText.qml:45
#, kde-format
msgid "Copy"
msgstr "Salin"

#: contents/ui/main.qml:27
#, kde-format
msgid "Networks"
msgstr "Jaringan"

#: contents/ui/main.qml:36
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn off Airplane Mode"
msgstr "Klik tengah untuk mematikan Mode Pesawat"

#: contents/ui/main.qml:38
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn on Airplane Mode"
msgstr "Klik tengah untuk menyalakan Mode Pesawat"

#: contents/ui/main.qml:67 contents/ui/Toolbar.qml:75
#, kde-format
msgid "Enable Wi-Fi"
msgstr "Aktifkan Wi-Fi"

#: contents/ui/main.qml:78
#, kde-format
msgid "Enable Mobile Network"
msgstr "Aktifkan Jaringan Seluler"

#: contents/ui/main.qml:90
#, kde-format
msgid "Enable Airplane Mode"
msgstr "Aktifkan Mode Pesawat"

#: contents/ui/main.qml:102
#, kde-format
msgid "Open Network Login Page…"
msgstr "Buka Halaman Masuk Jaringan..."

#: contents/ui/main.qml:112
#, kde-format
msgid "&Configure Network Connections…"
msgstr "&Konfigurasikan Koneksi Jaringan..."

#: contents/ui/PasswordField.qml:16
#, kde-format
msgid "Password…"
msgstr "Kata Sandi..."

#: contents/ui/PopupDialog.qml:51
#, fuzzy, kde-format
#| msgid "&Configure Network Connections…"
msgctxt "@action:button"
msgid "Return to Network Connections"
msgstr "&Konfigurasikan Koneksi Jaringan..."

#: contents/ui/ShareNetworkQrCodePage.qml:31
#, kde-format
msgid "Scan this QR code with another device to connect to the \"%1\" network."
msgstr ""

#: contents/ui/Toolbar.qml:124
#, kde-format
msgid "Enable mobile network"
msgstr "Aktifkan jaringan seluler"

#: contents/ui/Toolbar.qml:149
#, kde-kuit-format
msgctxt "@info"
msgid "Disable airplane mode<nl/><nl/>This will enable Wi-Fi and Bluetooth"
msgstr ""
"Nonaktifkan mode pesawat<nl/><nl/>Ini akan mengaktifkan Wi-Fi dan Bluetooth"

#: contents/ui/Toolbar.qml:150
#, kde-kuit-format
msgctxt "@info"
msgid "Enable airplane mode<nl/><nl/>This will disable Wi-Fi and Bluetooth"
msgstr ""
"Aktifkan mode pesawat<nl/><nl/>Ini akan menonaktifkan Wi-Fi dan Bluetooth"

#: contents/ui/Toolbar.qml:161
#, kde-format
msgid "Hotspot"
msgstr "Hotspot"

#: contents/ui/Toolbar.qml:185 contents/ui/Toolbar.qml:196
#, kde-format
msgid "Disable Hotspot"
msgstr "Nonaktifkan Hotspot"

#: contents/ui/Toolbar.qml:190 contents/ui/Toolbar.qml:196
#, kde-format
msgid "Create Hotspot"
msgstr "Buat Hotspot"

#: contents/ui/Toolbar.qml:228
#, kde-format
msgid "Configure network connections…"
msgstr "Konfigurasikan koneksi jaringan..."

#: contents/ui/TrafficMonitor.qml:35 contents/ui/TrafficMonitor.qml:106
#, kde-format
msgid "/s"
msgstr "/dtk"

#: contents/ui/TrafficMonitor.qml:87
#, kde-format
msgid "Upload"
msgstr "Unggah"

#: contents/ui/TrafficMonitor.qml:87
#, kde-format
msgid "Download"
msgstr "Unduh"

#~ msgctxt "@title:window"
#~ msgid "QR Code for %1"
#~ msgstr "Kode QR untuk %1"

#~ msgctxt "@info:tooltip %1 is the name of a network connection"
#~ msgid "Connect to %1"
#~ msgstr "Hubungkan ke %1"

#~ msgctxt "@info:tooltip %1 is the name of a network connection"
#~ msgid "Disconnect from %1"
#~ msgstr "Putuskan dari %1"

#~ msgctxt "text field placeholder text"
#~ msgid "Search…"
#~ msgstr "Cari..."

#~ msgctxt "button tooltip"
#~ msgid "Search the connections"
#~ msgstr "Cari koneksi"

#~ msgid ""
#~ "Connected, <font color='%1'>⬇</font> %2/s, <font color='%3'>⬆</font> %4/s"
#~ msgstr ""
#~ "Terkoneksi, <font color='%1'>⬇</font> %2/s, <font color='%3'>⬆</font> %4/s"

#~ msgid "Enable wireless"
#~ msgstr "Fungsikan nirkabel"

#~ msgid "Available connections"
#~ msgstr "Koneksi yang tersedia"

#~ msgid "General"
#~ msgstr "Umum"

#~ msgid "Ask for PIN on modem detection"
#~ msgstr "Tanya PIN saat deteksi modem"

#~ msgid "Show virtual connections"
#~ msgstr "Tampilkan koneksi virtual"
